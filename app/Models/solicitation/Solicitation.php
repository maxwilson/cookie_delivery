<?php

namespace App\Models\solicitation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitation extends Model
{
    use HasFactory;
    protected $table = 'solicitations';
    protected $fillable = ['client_id','product_id','status','observation','value'];
}
