<?php

namespace App\Http\Controllers\api\client;

use App\Http\Controllers\Controller;
use App\Models\client\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->getClientAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->ruleCreate($request);
        if(!empty($validation)) {
            return $validation;
        }

        $client = new Client();
        $client->name = $request->input('name');
        $client->phone = $request->input('phone');
        $client->cep = $request->input('cep');
        $client->logradouro = $request->input('logradouro');
        $client->number = $request->input('number');
        $client->complemento = $request->input('complemento');
        $client->bairro = $request->input('bairro');
        $client->localidade = $request->input('localidade');
        $client->uf = $request->input('uf');
        $client->save();

        return response()->json(['success'=>'Adicionado com Sucesso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $client = $this->getClientByID($id);

        return response()->json($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $validation = $this->ruleUpdate($request,$id);
        if(!empty($validation)) {
            return $validation;
        }

        $client = $this->getClientByID($id);

        if(!$client->isEmpty())  {
            $client = Client::where('id',$id);
            $client->update([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'cep' => $request->input('cep'),
                'logradouro' => $request->input('logradouro'),
                'number' => $request->input('number'),
                'complemento' => $request->input('complemento'),
                'bairro' => $request->input('bairro'),
                'localidade' => $request->input('localidade'),
                'uf' => $request->input('uf')
            ]);

            return response()->json(['success'=>'Atualizado com Sucesso']);
        }

        return response()->json(['error'=>'Dados não localizado'],404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $client = $this->getClientByID($id);

        if(!$client->isEmpty()) {
            Client::destroy($id);
            return response()->json(['success'=>'Excluido com Sucesso'],200);
        }else {
            return response()->json(['error'=>'Objeto não localizado'],404);
        }
    }

    public function searchCep(Request $request, $cep) {
        $cep = $request->cep;
        return $this->search_address($cep);
    }

    private function search_address($cep)
    {

        $response = Http::get('https://viacep.com.br/ws/'.$cep.'/json/');

        if($response->failed()) {
            return response()->json(['error'=>'Cep não localizado'],404);
        }

        return $response->json();
    }


    private function getClientByID($id)
    {
        return Client::where('id',$id)->get();
    }

    private function getClientAll()
    {
        return Client::all();
    }

    private function ruleCreate(Request $request)
    {
        $rules = [
            'name'            => 'required|min:10|max:160|string|unique:clients,name',
            'phone'           => 'required|unique:clients,phone',//adiciona regex
            'cep'             => 'required|numeric',
            'logradouro'      => 'required|string|min:10',
            'number'          => 'required|numeric',
            'complemento'     => 'nullable|string|max:160',
            'bairro'          => 'required|string',
            'localidade'      => 'required|string',
            'uf'              => 'required|string'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'O campo :attribute necessário no mínimo :min caracteres',
            'max' => 'O campo :attribute necessário no máximo :max caracteres',
        ];

        $validator = Validator::make($request->input(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    }

    private function ruleUpdate(Request $request, $id){

        $rules = [
            'name' => [
                'required','min:10','max:160','string',
                Rule::unique('clients','name')->ignore($id),
            ],
            'phone' => [
                'required','string','regex:/^([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})$/',
                Rule::unique('clients','phone')->ignore($id),
            ],
            'cep'             => 'required|numeric',
            'logradouro'      => 'required|string|min:10',
            'number'          => 'required|numeric',
            'complemento'     => 'nullable|string|max:160',
            'bairro'          => 'required|string',
            'localidade'      => 'required|string',
            'uf'              => 'required|string'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'O campo :attribute necessário no mínimo :min caracteres',
            'max' => 'O campo :attribute necessário no máximo :max caracteres',
            'cpf.regex' => 'O cpf é incorreto ou invalido',
        ];

        $validator = Validator::make($request->input(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    }
}
