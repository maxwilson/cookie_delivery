<?php

namespace App\Http\Controllers\api\solicitation;

use App\Http\Controllers\Controller;
use App\Models\solicitation\Solicitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SolicitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->getSolicitationByAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->rule($request);
        if(!empty($validation)) {
            return $validation;
        }

        $solicitation = new Solicitation();
        $solicitation->client_id = $request->input('client');
        $solicitation->product_id = $request->input('product');
        $solicitation->status = $request->input('status');
        $solicitation->observation = $request->input('observation');
        $solicitation->value = $request->input('value');
        $solicitation->save();

        return response()->json(['success'=>'Adicionado com Sucesso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $solicitation = $this->getSolicitationByID($id);

        return response()->json($solicitation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $validation = $this->rule($request);
        if(!empty($validation)) {
            return $validation;
        }

        $solicitation = $this->getSolicitationByID($id);

        if(!$solicitation->isEmpty())  {
            $solicitation = Solicitation::where('id',$id);
            $solicitation->update([
                'client_id' => $request->input('client'),
                'product_id' => $request->input('product'),
                'status' => $request->input('status'),
                'observation'=> $request->input('observation'),
                'value' => $request->input('value'),
            ]);

            return response()->json(['success'=>'Atualizado com Sucesso']);
        }

        return response()->json(['error'=>'Objeto não localizado'],404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $solicitation = $this->getSolicitationByID($id);

        if(!$solicitation->isEmpty()) {
            Solicitation::destroy($id);
            return response()->json(['success'=>'Excluido com Sucesso'],200);
        }else {
            return response()->json(['error'=>'Objeto não localizado'],404);
        }
    }

    private function getSolicitationByAll()
    {
        $solicitation = Solicitation::select(
            'solicitations.id','solicitations.client_id',
            'solicitations.product_id','solicitations.status',
            'solicitations.observation','solicitations.value',
            'clients.name', 'clients.phone',
            'products.name as product_name',
        )
        ->join('clients', function ($join) {
            $join->on('solicitations.client_id', '=', 'clients.id');
        })
        ->join('products', function ($join) {
            $join->on('solicitations.product_id', '=', 'products.id');
        })->get();

        return $solicitation;
    }

    private function getSolicitationByID($id)
    {
        $solicitation = Solicitation::select(
            'solicitations.id','solicitations.client_id',
            'solicitations.product_id','solicitations.status',
            'solicitations.observation','solicitations.value',
            'clients.name', 'clients.phone',
            'products.name as product_name',
        )
        ->join('clients', function ($join) {
            $join->on('solicitations.client_id', '=', 'clients.id');
        })
        ->join('products', function ($join) {
            $join->on('solicitations.product_id', '=', 'products.id');
        })->where('solicitations.id',$id)->get();

        return $solicitation;
    }

    private function rule(Request $request)
    {

        $rules = [
            'product'         => 'required|numeric',
            'status'          => 'required|in:aberto,fechado',
            'observation'     => 'required|min:10|max:160|string',
            'value'           => 'required|numeric',
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'O campo :attribute necessário no mínimo :min caracteres',
            'max' => 'O campo :attribute necessário no máximo :max caracteres',
        ];

        $validator = Validator::make($request->input(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    }
}
