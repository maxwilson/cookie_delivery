<?php

namespace App\Http\Controllers\api\product;

use App\Http\Controllers\Controller;
use App\Models\product\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->getProductAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->ruleCreate($request);
        if(!empty($validation)) {
            return $validation;
        }

        $product = new Product();
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->quantity = $request->input('quantity');
        $product->save();

        return response()->json(['success'=>'Adicionado com Sucesso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $product = $this->getProductByID($id);

        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $validation = $this->ruleUpdate($request,$id);
        if(!empty($validation)) {
            return $validation;
        }

        $product = $this->getProductByID($id);

        if(!$product->isEmpty())  {
            $product = Product::where('id',$id);
            $product->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'quantity' => $request->input('quantity'),
            ]);

            return response()->json(['success'=>'Atualizado com Sucesso']);
        }

        return response()->json(['error'=>'Objeto não localizado'],404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $product = $this->getProductByID($id);

        if(!$product->isEmpty()) {
            Product::destroy($id);
            return response()->json(['success'=>'Excluido com Sucesso'],200);
        }else {
            return response()->json(['error'=>'Objeto não localizado'],404);
        }
    }

    private function getProductByID($id)
    {
        return Product::where('id',$id)->get();
    }

    private function getProductAll()
    {
        return Product::all();
    }

    private function ruleCreate(Request $request)
    {

        $rules = [
            'name'            => 'required|min:10|max:100|string|unique:products,name',
            'description'     => 'nullable|min:10|max:120',
            'quantity'        => 'required|min:10|max:160|numeric',
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'O campo :attribute necessário no mínimo :min caracteres',
            'max' => 'O campo :attribute necessário no máximo :max caracteres',
        ];

        $validator = Validator::make($request->input(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    }

    private function ruleUpdate(Request $request, $id)
    {
        $rules = [
            'name' => [
                'required','min:10','max:100','string',
                Rule::unique('products','name')->ignore($id),
            ],
            'description'   => 'nullable|min:10|max:120',
            'quantity'      => 'required|min:10|max:160|numeric',
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'O campo :attribute necessário no mínimo :min caracteres',
            'max' => 'O campo :attribute necessário no máximo :max caracteres',
        ];

        $validator = Validator::make($request->input(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    }

}
