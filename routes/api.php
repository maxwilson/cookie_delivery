<?php

use App\Http\Controllers\api\client\ClientController;
use App\Http\Controllers\api\product\ProductController;
use App\Http\Controllers\api\solicitation\SolicitationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/search/cep/{cep}', [ClientController::class, 'searchCep']);

/* Client */
Route::apiResource('/clients', ClientController::class);
/* Product */
Route::apiResource('/products', ProductController::class);
/* solicitations */
Route::apiResource('solicitations', SolicitationController::class);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
